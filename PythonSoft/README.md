# Read ME

## What's dis?

This is `/software/PythonSoft` where I put software that I want which will mostly be python stuff. My programs are NOTHING special and there are just better versions of programs that 
already exist. The names may be inaccurate for the program although I will try to organize it the best I can.

PythonSoft is a mix of both ideas and real-world use programs and are sorted using folder names like "Utilities", "Games" and else. 

## Who Am I?

I'm Okmeque1,who used to be lone coder who does mostly stuff that already exists in my own fashion (like the way that passwords are saved is by having a set name written to a save file.)Don't ask me for specific needs as I do stuff on my own, no need for help there. I started Python towards ~early 2022 and the first programs I made were TERRIBLE. I then continued to improve using physical help, internet help and now modern AI-Based help.(To clarify, I am **NOT** using AI to make my programs, unless it is in the `Software/PythonSoft/ChatGPT` folder where it is explicitly mentioned) and some of my more modern programs are much cleaner and use new modules. 

## How to CHANGE the code : 

If you plan on updating my code,please make sure to include at the VERY start of the program who updated it,on what date and time and what changed and do **NOT** commit to the MAIN branch. Create a new branch for your code and start a **PULL REQUEST**, which is located under the word SOFTWARE. This is important as it allows collaborators to see what has changed and what can and needs to be changed. 

## Useful information : 

Before downloading any programs,MAKE SURE TO READ [the license terms](https://github.com/Okmeque1/software/blob/main/LICENSE.md) for licensing of those programs.

For a short description of the program,see COMMIT log or [the program chart](https://github.com/Okmeque1/software/blob/main/Programs.md)

For upcoming and made updates,see [the update chart](https://github.com/Okmeque1/software/blob/main/UPDATE.MD)

If a program fails to run, please refer to [the error chart](https://github.com/Okmeque1/software/blob/main/PythonSoft/errors.md)

WARNING! The UPDATE.MD is not regularly updated and will NOT include small changes. Please refer to the commit log to see the full extent of my progress.

The programs listed above are not DESIGNED for commercial use but could be used for commercial use if needed. E-mail okmeque1@gmail.com

## Collaborators : 

There are 2 Collaborators on this GitHub repository. 

1 - Okmeque1

2 - GamerSoft24

Warning! **NO** demands for `collaborator role` will be accepted for trust and safety reasons. If you want to update the code, fork this and pull request it or send the code to my email(okmeque1@gmail.com).


## Required libraries

To install the requisite libraries, navigate to your Python INSTALL folder by opening the search bar, typing python and opening the file location of PYTHON.EXE. Then navigate to the scripts folder using command prompt and then do 'pip install [modulename]'

You need : 

1 : RANDOM


2 : TIME


3 : OpenCV(2)


4 : Cryptography


5 : OS


6 : tKinter


7 : Python Turtle graphics


8 : PyQT 5 - Windows 7 Compatible


9 : PSUTIL


10 : REQUESTS
