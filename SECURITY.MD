# Program Security and Change Policy

## Program Security : 

All programs in this repository are meant to be used safely with precaution and all are mostly safe to use. Any programs that may affect the usage of a computer are clearly marked with comments or marked in the README. Some programs like iCMD are in a gray zone, as they do not interfere directly but as a command line, some people might use it maliciously.

## Change Policy : 

If you want to change a program, **NEVER COMMIT TO THE MAIN BRANCH**. Instead make a *pull request* located under the word `software` on most devices. This will be reviewed within a week and discussions about the changes could occur.

If you want to mark out an issue, please make an issue, located under the work `Okmeque1`. You must **CLEARLY** point where the issue is as it's not my job to sift through issues and wondering 'wtf is the issue?', and the issue will be reviewed in a week and discussions about how to fix it could occur.


## Others : 

Anyone seen violating the code-of-conduct can be reported at `okmeque1@gmail.com`. However, in-depth detail must be provided in order that the report is any useful.
