# Files

## What for? : 

If you have trouble finding the requisite `files`, you can search the folder name and where it is located. This chart is repository-wide, meaning that you can search the programs for every folder

## PythonSoft : 

### Non-Code Files : 

1 : [PROGRAMS.MD - Program Chart](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Programs.md)

2 : [LICENSE.MD - License Terms](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/LICENSE.md)

3 : [README.MD - The Read Me](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/README.md)

4 : [UPDATE.MD - Update chart with major updates](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/UPDATE.MD)

5 : [COMPTEST.MD - Compatability Chart](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/COMPTEST.md)

6 : [ERRORS.MD - Error chart](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/errors.md)

### Utilities :

1 : [PWD.PY - Password Manager](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/pwd.py)

2 : Encryption suite : [PWD-HASH_EDITION.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/pwd-hash_edition.py) - [ENCRYPTER-DECRYPTER.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/Encrypter-Decrypter.py) - [KEYGEN.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/keygen.py) : Useful for hiding stuff

3 : [PWD_CHECKER.PY : Password strength checker.](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/pwd_checker.py)

4 : [FILE MANAGER.PY : File Explorer in CMDLine](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/file%20manager.py)

5 : iCMD : [iCMD.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/iCMD.py) - [iCMD-Lite](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/iCMD-Lite.py) : Use if CMD/Terminal is blocked

6 : [BCONVERTER.PY : Binary Converter (INT ONLY)](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/bconverter.py)

7 : [IMAGE DISPLAYER.PY : Use if the photos app is broken too](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/image%20displayer.py) 

8 : [WORD PROCESSOR.PY : Procceses the word in a text file and tell you what word/position if is in.](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/word%20processor.py)

9 : [FACT.PY : Factorial numbers - Broken](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/fact.py)

10 : [FIGURE.PY : Draws a figure based on the numbers of sides.3 = Triangle,4 = Square and whatnot.](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/figure.py)

11 : [CLOCK.PY : Digital clock. Can't you understand?](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Utilities/clock.py)

### Games :

1 : [CONNECT4.PY : Connect 4.](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/connect4.py)

2 : [COUNTING#5250.PY : Test your counting skills](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/counting#5250.py)

3 : [DOS.PY : Joke DOS emulator](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/dos.py)

4 : [FILLER.PY : Make a large file to impress someone](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/filler.py)

5 : HANGMAN : [HANGMAN.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/hangman.py) - [UNHANGMAN.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/unhangman.py) : One works more than the other

6 : [NOT 1.PY : Try for yourself](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/not%201.py)

7 : [TICTACTOE.PY : Tic,Tac,Toe.](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/tictactoe.py)

8 : RPS : [RPS-Okmeque1_Edition.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/rps-okmeque1_edition.py) - [RPS-Compact_edition.PY](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/rps-compact_edition.py) - One is slightly more broken than the other

9 : [OPENTDB.PY : Test your knowledge!](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/Games/opentdb.py)

### Non-descript files(Links not included as they are mostly broken or useless) : 

1 : LOGIN.PY : Basic log-in system.

2 : TEXT EDITOR.PY : Do I need to explain?

### ChatGPT :

1 : [GUI-PWD.PY : PWD.PY but with GUI goodness - Written with ChatGPT](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/ChatGPT/GUI-PWD.py)

2 : [G-Editor-PyQt_edition.PY : PyQt G-Editor - Written with ChatGPT](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/ChatGPT/G-Editor%20-%20PyQT%20ChatGPT%20Edition.py)

### GUI : 

1 : [GUI-PWD.PY : GUI-PWD Okmeque1 Real edition - Written in tkinter Python in Okmeque1 Style](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/GUI/GUI-PWD.py)

2 : [G-Editor.PY : GUI Text editor that will replace TEXT EDITOR.PY. Text editor is now deprecated and no longer going to receive updates](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/GUI/G-Editor.py)

3 : [G-Calc.PY : Basic calculator](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/GUI/G-Calc.py)

4 : [Error generator.PY : Need I say more?](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/PythonSoft/GUI/Error%20generator.py)

## WinScriptHost : 

1 : [README](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/README.MD) : Short description of the folder.

2 : [All.vbs](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/all.vbs) : All of the MSGBOXES possible using VBS

3 : [CDROM Drive No delay.vbs](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/cdrom%20drive%20no%20delay.vbs) : Ejects a CD-ROM drive in a manner that when you close it, the drive will re-open. This effect only works on SlimLine drives(where you can see the laser and you attach the disc directly to the motor) as they cannot retract their tray.

4 : [CDROM Drive.vbs](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/cdrom%20drive.vbs) : Ejects a CD-ROM drive in a manner that when you close it, the drive will re-open after a set period of time. This effect is great as the drive will seemingly open randomly but only works on SlimLine drives(where you can see the laser and you attach the disc directly to the motor) as they cannot retract their tray.

5 : [Logon.vbs](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/logon.VBS) : Login simulator in VBS. For this script to work properly, download file 6 (Logon2.vbs) and download it in the same directory as this downloaded script. This is the user-name script out of the 2 scripts

6 : [Logon2.vbs](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/WinScriptHost/logon2.VBS) : Login simulator in VBS. This is the password script out of the 2 scripts

## Batch&CMD : 

### DestroyPC : 

[START1.BAT](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/Batch%26CMD/DestroyPC/START1.BAT) : Script that starts itself which then starts itself and then starts itself and then starts itself and then starts itself and then starts itself and i can't take it anymore. Challenge : Try and say it

### Utilities : 

[WinBatch360 AIO Software.bat](https://gitlab.com/GS-ST/Okmeque1/-/blob/main/Batch%26CMD/Utilities/WinBatch360%20AIO%20Software.bat) : Includes functions such as UAC bypass and command flag switches and to top it all off you have an option to start a browser with lots of compatability command switches.

[MAS_1.5 Folder](https://github.com/Okmeque1/software/tree/main/Batch%26CMD/Utilities/MAS_1.5) : I didn't make it, my advice is just use the AIO version unless it ain't working. For the seperate file versions you figure it out cuz you have a brain.
